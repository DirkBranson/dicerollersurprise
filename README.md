# DiceRollerSurprise


## Name
DiceRollerSurpise

## Description
This learning-project is capable of simulating the roll of a cursed dice.... You've got a 1/7 chance of rolling any given number, and a 1/7 chance of a seriously spooky surprise.....


## Visuals

## Installation
Download the files and build on Android Studio Flamingo or later.

Press button, observe output.

## Support
dunnettjalex@gmail.com  

## Roadmap
A step on a very long road.

## Contributing
I'd rather you didn't

## Authors and acknowledgment
Just me with a lot of help from the Android Compose Training Team.

## License
For open source projects, say how it is licensed.

## Project status
Complete.